module gitlab.com/atcheri/go-finance-api

go 1.15

require (
	github.com/alpeb/go-finance v0.0.0-20201203160833-c5b5ec452a2e
	github.com/apex/gateway v1.1.2
	github.com/stretchr/testify v1.6.1
)
