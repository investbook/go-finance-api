## Install dependencies
```sh
go install
```

## Run server locally
```sh
./run.sh
```

## Run tests
```sh
go test ./...
```

## Build
```sh
./build.sh
```


References:
 - [netlify deploy](https://blog.carlmjohnson.net/post/2020/how-to-host-golang-on-netlify-for-free/)
 - [error handling](https://medium.com/@ozdemir.zynl/rest-api-error-handling-in-go-behavioral-type-assertion-509d93636afd)
