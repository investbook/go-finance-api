package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCashFlow(t *testing.T) {
	t.Run("[ERROR] returns an error with empty input", func(t *testing.T) {
		instance, err := NewCashFlow(nil)
		assert.Nil(t, instance)
		assert.NotNil(t, err)
		assert.EqualError(t, err, empty_input_err_msg)
	})
	t.Run("[ERROR] returns an error with invalid input", func(t *testing.T) {
		instance, err := NewCashFlow([]string{"40", "invalid", "another-invalid"})
		assert.Nil(t, instance)
		assert.NotNil(t, err)
		assert.EqualError(t, err, invalid_cash_flow_input_err_msg)
	})
	t.Run("[NO ERROR] returns an error with invalid input", func(t *testing.T) {
		instance, err := NewCashFlow([]string{"40", "50", "60"})
		assert.NotNil(t, instance)
		assert.Nil(t, err)
		assert.EqualValues(t, []float64{40, 50, 60}, instance.Flow)
	})
}
