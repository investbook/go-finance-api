package main

import (
	"errors"
	"strconv"

	"github.com/alpeb/go-finance/fin"
)

// CashFlow is
type CashFlow struct {
	Flow []float64 `json:"cash-flow"`
}

var (
	empty_input_err_msg             = "Empty input cash flow"
	invalid_cash_flow_input_err_msg = "Error while parsing input cash flow"
)

// NewCashFlow is
func NewCashFlow(inputFlow []string) (*CashFlow, error) {
	if inputFlow == nil {
		return nil, errors.New(empty_input_err_msg)
	}
	flow := make([]float64, 0)
	for _, cash := range inputFlow {
		val, err := strconv.ParseFloat(cash, 64)
		if err != nil {
			return nil, errors.New(invalid_cash_flow_input_err_msg)
		}
		flow = append(flow, val)
	}
	return &CashFlow{Flow: flow}, nil
}

func (cf *CashFlow) InternalRateOfReturn() (float64, error) {
	return fin.InternalRateOfReturn(cf.Flow, 0)
}
