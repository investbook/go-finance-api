package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {
	t.Run("[ERROR] 400 when no query parameters", func(t *testing.T) {
		req, err := http.NewRequest("GET", api_irr_url, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler := http.Handler(rootHandler(handler))

		handler.ServeHTTP(rr, req)

		assert.Equal(t, http.StatusBadRequest, rr.Code)

		assert.Equal(t, "{\"detail\":\"Bad request : missing query parameter.\"}", rr.Body.String())
	})
	t.Run("[ERROR] 400 for invalid cash-flow parameter", func(t *testing.T) {
		req, err := http.NewRequest("GET", fmt.Sprintf("%s/?cash-flow=ertrr", api_irr_url), nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler := http.Handler(rootHandler(handler))

		handler.ServeHTTP(rr, req)

		assert.Equal(t, http.StatusBadRequest, rr.Code)

		assert.Equal(t, fmt.Sprintf("{\"detail\":\"%s\"}", invalid_cash_flow_input_err_msg), rr.Body.String())
	})
	t.Run("[ERROR] 400 for cash-flow parameters without a negative value", func(t *testing.T) {
		req, err := http.NewRequest("GET", fmt.Sprintf("%s/?cash-flow=10&cash-flow=20", api_irr_url), nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler := http.Handler(rootHandler(handler))

		handler.ServeHTTP(rr, req)

		assert.Equal(t, http.StatusPreconditionFailed, rr.Code)

		assert.Equal(t, "{\"detail\":\"the cash flow must contain at least one positive value and one negative value\"}", rr.Body.String())
	})
	t.Run("[NO ERROR] returns json data having a rate field  with a float value", func(t *testing.T) {
		req, err := http.NewRequest("GET", fmt.Sprintf("%s/?cash-flow=-10&cash-flow=20&cash-flow=30", api_irr_url), nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler := http.Handler(rootHandler(handler))

		handler.ServeHTTP(rr, req)

		assert.Equal(t, http.StatusOK, rr.Code)
		assert.Equal(t, "application/json", rr.Header().Get("Content-Type"))
		assert.Equal(t, "{\"rate\":2.0000000000000004}\n", rr.Body.String())
	})
}
