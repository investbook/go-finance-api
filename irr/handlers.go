package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// RateResponse contains the returned IRR as float64 value
type RateResponse struct {
	Rate float64 `json:"rate"`
}

// ClientError is an error whose details to be shared with client.
type ClientError interface {
	Error() string
	// ResponseBody returns response body.
	ResponseBody() ([]byte, error)
	// ResponseHeaders returns http status code and headers.
	ResponseHeaders() (int, map[string]string)
}

type rootHandler func(http.ResponseWriter, *http.Request) error

func (fn rootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := fn(w, r) // Call handler function
	if err == nil {
		return
	}
	// This is where our error handling logic starts.
	log.Printf("An error accured: %v", err) // Log the error.

	clientError, ok := err.(ClientError) // Check if it is a ClientError.
	if !ok {
		// If the error is not ClientError, assume that it is ServerError.
		w.WriteHeader(500) // return 500 Internal Server Error.
		return
	}

	body, err := clientError.ResponseBody() // Try to get response body of ClientError.
	if err != nil {
		log.Printf("An error accured: %v", err)
		w.WriteHeader(500)
		return
	}
	status, headers := clientError.ResponseHeaders() // Get http status code and headers.
	for k, v := range headers {
		w.Header().Set(k, v)
	}
	w.WriteHeader(status)
	w.Write(body)
}

func handler(w http.ResponseWriter, r *http.Request) error {
	params := r.URL.Query()
	flow, ok := params["cash-flow"]
	if !ok {
		return NewHTTPError(nil, http.StatusBadRequest, "Bad request : missing query parameter.")
	}

	cashFlow, err := NewCashFlow(flow)
	if err != nil {
		return NewHTTPError(nil, http.StatusBadRequest, err.Error())
	}

	irr, err := cashFlow.InternalRateOfReturn()
	if err != nil {
		return NewHTTPError(nil, http.StatusPreconditionFailed, err.Error())
	}

	// resp := RateResponse{Rate: irr}
	// body, err := json.Marshal(resp)
	// if err != nil {
	// 	return NewHTTPError(nil, http.StatusInternalServerError, err.Error())
	// }

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	// w.Write(body)
	json.NewEncoder(w).Encode(RateResponse{Rate: irr})
	return nil
}
